# 远程调用Application Demo
接收Actix-web传来的请求， 通过Zeromq的REQ+REP套接字模式，在本地查询数据，将数据原路返回
#### 环境
rust 1.64

Ubuntu 20.04.4 LTS

Clickhouse 22.11.1.1360

2022-11-28 17：57 更新
此脚本被封装成一个Application， 供Actix来远程调用